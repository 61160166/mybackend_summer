const Admin = require('../models/Admin')
const adminController = {
  adminList: [],

  async addAdmin (req, res) {
    const payload = req.body
    const admins = new Admin(payload)
    try {
      await admins.save()
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateAdmin (req, res) {
    const payload = req.body
    try {
      const admins = await Admin.updateOne({ _id: payload._id }, payload)
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteAdmin (req, res) {
    const { id } = req.params
    try {
      const admins = await Admin.deleteOne({ _id: id })
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getAdmins (req, res) {
    try {
      const admins = await Admin.find({})
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getAdmin (req, res) {
    const { id } = req.params

    try {
      const admins = await Admin.findById(id)
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = adminController
