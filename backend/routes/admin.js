const express = require('express')
const router = express.Router()
const adminController = require('../Controller/AdminController')

router.get('/', adminController.getAdmins)

router.get('/:id', adminController.getAdmin)

router.post('/', adminController.addAdmin)

router.put('/', adminController.updateAdmin)

router.delete('/:id', adminController.deleteAdmin)

module.exports = router
