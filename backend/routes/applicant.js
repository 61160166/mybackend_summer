const express = require('express')
const router = express.Router()
const applicantsController = require('../controller/ApplicantsController')

const mongoose = require('mongoose')
mongoose.connect('/mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error: '))
db.once('open', function () {
  console.log('connect')
})

/* GET users listing. */
router.get('/', applicantsController.getApplicants)

router.get('/:id', applicantsController.getApplicant)

router.post('/', applicantsController.addApplicant)

router.put('/', applicantsController.updateApplicant)

router.delete('/:id', applicantsController.deleteApplicant)

module.exports = router
