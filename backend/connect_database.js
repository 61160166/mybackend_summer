const mongoose = require('mongoose')
const Applicant = require('./models/Applicant')
const Admin = require('./models/Admin')

mongoose.connect('/mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error: '))
db.once('open', function () {
  console.log('connect')
})

Applicant.find(function (err, applicants) {
  if (err) return console.error(err)
  console.log(applicants)
  return applicants
})

Admin.find(function (err, admins) {
  if (err) return console.log(err)
  console.log(admins)
  return admins
})