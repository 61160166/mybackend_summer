const mongoose = require('mongoose')
const Schema = mongoose.Schema
const adminSchema = new Schema({
  first_name: String,
  last_name: String,
  gender: String,
  birthday: String,
  tel: String
})

module.exports = mongoose.model('Admin', adminSchema)
