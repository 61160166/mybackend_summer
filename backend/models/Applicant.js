// const { urlencoded } = require('express')
const mongoose = require('mongoose')

// const Schema = mongoose.Schema
const applicantSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  gender: String,
  birthday: String,
  nationality: String,
  tel: String,
  address: String
})

module.exports = mongoose.model('Applicant', applicantSchema)
